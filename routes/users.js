// Router

var express = require("express");
var router = express.Router();
const { check, validationResult } = require("express-validator");
var passport = require("passport");
var LocalStrategy = require('passport-local').Strategy;

var User = require('../model/user');

/* GET users listing. */
router.get("/", (req, res, next) => {
  res.send("respond with a resource");
});

router.get("/register", (req, res, next) => {
  res.render("register");
});

router.get("/login", (req, res, next) => {
  res.render("login");
});

router.get("/logout", (req, res) => {
  req.logOut();
  res.redirect('/users/login');
});

/* POST users listing. */
router.post(
  "/register",
  [
    check("email", "กรุณากรอกอีเมล์").isEmail(),
    check("name", "กรุณากรอกชื่อ").not().isEmpty(),
    check("password", "กรุณากรอกรหัสผ่าน").not().isEmpty()
  ],(req, res, next) => {
    const result = validationResult(req);
    var errors = result.errors;
    //Validation Data
    if (!result.isEmpty()) {
      //Return error to view
      res.render("register", { errors: errors });
    } else {
      //Insert Data
      var name = req.body.name;
      var password = req.body.password;
      var email = req.body.email;
      var phone_number = req.body.phone_number
      var newUser = new User({
        name: name,
        password: password,
        email: email,
        phone_number: phone_number
      });
      User.createUser(newUser, (err, user) => {
        if(err) throw err
      });
      res.location('/');
      res.redirect('/');
    }
  }
);

router.post('/login', passport.authenticate('local', {
  failureRedirect: '/users/login',
  failureFlash: true
}), (req, res) => {
  req.flash("success", "ลงชื่อเข้าใช้เรียบร้อยแล้ว");
  res.redirect('/');
});

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  User.getUserById(id, (err, user) => {
    done(err, user);
  });
});

passport.use(new LocalStrategy((username, password, done) => {
  User.getUserByName(username, (err, user) => {
    if (err) throw error
    if (!user) {
      return done(null, false);
    }
    return User.comparePassword(password, user.password, (err, isMatch) => {
      if (err) throw error
      if (!isMatch) {
        return done(null, false);
      } else {
        return done(null, user); 
      }
    });
  });
}
));

module.exports = router;

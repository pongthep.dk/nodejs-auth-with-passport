//Model
var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
const passport = require('passport');

var mongoDB = 'mongodb://localhost:27017/LoginDB';

mongoose.connect(mongoDB, {
    useNewUrlParser:true
});
//Connect
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB Connect Error'));

//Create Schema
var userSchema = mongoose.Schema({
    name: {
        type:String
    },
    email: {
        type:String
    },
    password: {
        type:String
    },
    phone_number: {
        type:String
    }
});

var User = module.exports = mongoose.model('Users', userSchema);

module.exports.createUser = (newUser, callBack) => {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
            newUser.password = hash;
            newUser.save(callBack);
        });
    });
}

module.exports.getUserById = (id, callBack) => {
    User.findById(id, callBack);
}

module.exports.getUserByName = (name, callBack) => {
    var query = {
        name: name,
    }
    User.findOne(query, callBack);
}

module.exports.comparePassword = (password, hash, callBack) => {
    bcrypt.compare(password, hash, (err, isMatch) => {
        callBack(null, isMatch);
    });
}